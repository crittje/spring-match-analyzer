package com.chris.matchanalyzer.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String tournamentGroup;

//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
//    private List<Player> players = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Match> matches = new HashSet<>();

    /**
     * Getters and setters
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTournamentGroup() {
        return tournamentGroup;
    }

    public void setTournamentGroup(String tournamentGroup) {
        this.tournamentGroup = tournamentGroup;
    }

    public Set getMatches() {
        return matches;
    }

    public void setHomeMatches(Set homeMatches) {
        this.matches = homeMatches;
    }
}
