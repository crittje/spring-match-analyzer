package com.chris.matchanalyzer.domain;

import javax.persistence.*;

@Entity
public class Events {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer minute;

    @OneToOne
    private Player player;

    private Boolean goal;

    private Boolean ownGoal;

    private Boolean yellowCard;

    private Boolean redCard;

    private Boolean assist;

    private Boolean substitutionIn;

    private Boolean substitutionOut;

    @OneToOne
    private Match match;

    /**
     * Getters and setters
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Boolean getGoal() {
        return goal;
    }

    public void setGoal(Boolean goal) {
        this.goal = goal;
    }

    public Boolean getOwnGoal() {
        return ownGoal;
    }

    public void setOwnGoal(Boolean ownGoal) {
        this.ownGoal = ownGoal;
    }

    public Boolean getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(Boolean yellowCard) {
        this.yellowCard = yellowCard;
    }

    public Boolean getRedCard() {
        return redCard;
    }

    public void setRedCard(Boolean redCard) {
        this.redCard = redCard;
    }

    public Boolean getAssist() {
        return assist;
    }

    public void setAssist(Boolean assist) {
        this.assist = assist;
    }

    public Boolean getSubstitutionIn() {
        return substitutionIn;
    }

    public void setSubstitutionIn(Boolean substitutionIn) {
        this.substitutionIn = substitutionIn;
    }

    public Boolean getSubstitutionOut() {
        return substitutionOut;
    }

    public void setSubstitutionOut(Boolean substitutionOut) {
        this.substitutionOut = substitutionOut;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
