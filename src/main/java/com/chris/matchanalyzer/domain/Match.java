package com.chris.matchanalyzer.domain;

import javax.persistence.*;

@Entity
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer round;

    private String stadiumName;

    private Integer stadiumCapacity;

    private Integer visitors;

    private Long homeTeamId;

    private Long awayTeamId;

    private Long homeTeamGoals;

    private Long awayTeamGoals;

    /**
     * Getters and setters
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }

    public String getStadiumName() {
        return stadiumName;
    }

    public void setStadiumName(String stadiumName) {
        this.stadiumName = stadiumName;
    }

    public Integer getStadiumCapacity() {
        return stadiumCapacity;
    }

    public void setStadiumCapacity(Integer stadiumCapacity) {
        this.stadiumCapacity = stadiumCapacity;
    }

    public Integer getVisitors() {
        return visitors;
    }

    public void setVisitors(Integer visitors) {
        this.visitors = visitors;
    }

    public Long getHomeTeamId() {
        return homeTeamId;
    }

    public void setHomeTeamId(Long homeTeamId) {
        this.homeTeamId = homeTeamId;
    }

    public Long getAwayTeamId() {
        return awayTeamId;
    }

    public void setAwayTeamId(Long awayTeamId) {
        this.awayTeamId = awayTeamId;
    }

    public Long getHomeTeamGoals() {
        return homeTeamGoals;
    }

    public void setHomeTeamGoals(Long homeTeamGoals) {
        this.homeTeamGoals = homeTeamGoals;
    }

    public Long getAwayTeamGoals() {
        return awayTeamGoals;
    }

    public void setAwayTeamGoals(Long awayTeamGoals) {
        this.awayTeamGoals = awayTeamGoals;
    }
}
