package com.chris.matchanalyzer.repositories;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player, Long>{

    @Query("Select p from Player p where p.name = ?1")
    Optional<Player> findByName(String name);

//    @Query("Select m from Match m where m.player.id = ?1")
//    List<Match> getMatchesByPlayerId(long id);



}
