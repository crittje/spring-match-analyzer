package com.chris.matchanalyzer.repositories;

import com.chris.matchanalyzer.domain.Match;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchRepository extends JpaRepository<Match, Long> {
}
