package com.chris.matchanalyzer.repositories;

import com.chris.matchanalyzer.domain.Events;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EventsRepository  extends  JpaRepository<Events, Long> {

    @Query("Select e from Events e where e.player.id = ?1")
    List<Events> findPlayerEventsById(long id);
}
