package com.chris.matchanalyzer.repositories;

import com.chris.matchanalyzer.domain.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
