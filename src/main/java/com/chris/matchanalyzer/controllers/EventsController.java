package com.chris.matchanalyzer.controllers;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.services.EventsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EventsController {

    private EventsService eventsService;

    public EventsController(EventsService eventsService) {
        this.eventsService = eventsService;
    }

    @GetMapping("/events/player_id/{id}")
    public List<Events> getEventsByPlayerId(@PathVariable long id) {
        return eventsService.getEventsByPlayerId(id);
    }
}
