package com.chris.matchanalyzer.controllers;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.services.PlayerService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class PlayerController {

    private PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @RequestMapping("/players/all_details")
    public List<Player> getPlayersAllDetails() {
        return playerService.getAllDetails();
    }

    @RequestMapping("/players/id/{id}")
    public Optional<Player> getPlayerById(@PathVariable Long id) {
        return playerService.getPlayerById(id);
    }

    @RequestMapping("/players/name/{name}")
    public Optional<Player> getPlayerByName(@PathVariable String name) {
        return playerService.getPlayerByName(name);
    }

}
