package com.chris.matchanalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchAnalyzerApplication.class, args);
	}
}
