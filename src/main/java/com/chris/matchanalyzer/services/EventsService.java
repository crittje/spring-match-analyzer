package com.chris.matchanalyzer.services;

import com.chris.matchanalyzer.domain.Events;

import java.util.List;

public interface EventsService {

    List<Events> getEventsByPlayerId(long id);
}
