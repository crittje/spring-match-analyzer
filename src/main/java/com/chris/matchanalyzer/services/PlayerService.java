package com.chris.matchanalyzer.services;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Player;

import java.util.List;
import java.util.Optional;

public interface PlayerService {

    List<Player> getAllDetails();

    Optional<Player> getPlayerById(long id);

    Optional<Player> getPlayerByName(String name);

    Optional<List<Events>> getPlayerMatchesByPlayerId(long id);

    Player getPlayerMatchesByPlayerName();

}
