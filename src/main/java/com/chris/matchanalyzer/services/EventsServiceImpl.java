package com.chris.matchanalyzer.services;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.repositories.EventsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventsServiceImpl implements EventsService {

    private EventsRepository eventsRepository;

    public EventsServiceImpl(EventsRepository eventsRepository) {
        this.eventsRepository = eventsRepository;
    }

    @Override
    public List<Events> getEventsByPlayerId(long id) {
        return eventsRepository.findPlayerEventsById(id);
    }
}
