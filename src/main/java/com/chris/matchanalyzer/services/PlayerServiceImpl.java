package com.chris.matchanalyzer.services;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.repositories.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService{

    private PlayerRepository playerRepository;

    PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public List<Player> getAllDetails() {
        return playerRepository.findAll();
    }

    public Optional<Player> getPlayerById(long id) {
        return playerRepository.findById(id);
    }

    @Override
    public Optional<Player> getPlayerByName(String name) {

        return playerRepository.findByName(name);
    }

    @Override
    public Optional<List<Events>> getPlayerMatchesByPlayerId(long id) {


        return null;
    }

    @Override
    public Player getPlayerMatchesByPlayerName() {
        return null;
    }

}
