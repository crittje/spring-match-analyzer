package com.chris.matchanalyzer.bootstrap;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Match;
import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.domain.Team;
import com.chris.matchanalyzer.repositories.EventsRepository;
import com.chris.matchanalyzer.repositories.MatchRepository;
import com.chris.matchanalyzer.repositories.PlayerRepository;
import com.chris.matchanalyzer.repositories.TeamRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent>{

    private PlayerRepository playerRepository;
    private TeamRepository teamRepository;
    private MatchRepository matchRepository;
    private EventsRepository eventsRepository;
    private Team teamOne;
    private Team teamTwo;
    private Match matchOne;
    private Player playerOne;

    public DevBootstrap(PlayerRepository playerRepository, TeamRepository teamRepository, MatchRepository matchRepository,
                        EventsRepository eventsRepository) {
        this.playerRepository = playerRepository;
        this.teamRepository = teamRepository;
        this.matchRepository = matchRepository;
        this.eventsRepository = eventsRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

        loadTeams();
        loadPlayers();
        loadMatches();
        loadEvents();

    }

    private void loadTeams() {

        teamOne = createTeam("Portugal", "A");
        teamRepository.save(teamOne);

        teamTwo = createTeam("Argentina", "B");
        teamRepository.save(teamTwo);
    }

    private Team createTeam(String name, String group) {

        Team team = new Team();
        team.setName(name);
        team.setTournamentGroup(group);

        return team;

    }

    private void loadPlayers() {

        playerOne = createPlayer("C. Ronaldo", teamOne);
        playerRepository.save(playerOne);

        Player playerTwo = createPlayer("L. Messi", teamTwo);
        playerRepository.save(playerTwo);

        Player playerThree = createPlayer("Maradonna", teamTwo);
        playerRepository.save(playerThree);
    }

    private Player createPlayer(String name, Team team) {

        Player player = new Player();
        player.setName(name);
        player.setTeam(team);

        return player;
    }

    private void loadMatches() {

        matchOne = createMatch(1L, 2L, 1L, 2L, 1, "Test Stadium",
                40000, 37000);
        matchRepository.save(matchOne);

        Match matchTwo = createMatch(2L, 1L, 2L, 1L, 2, "Mooi stadium",
                30000, 27000);
        matchRepository.save(matchTwo);


        Match matchThree = createMatch(1L, 2L, 1L, 1L, 1, "Euroborg",
                20000, 10000);
        matchRepository.save(matchThree);

        teamOne.getMatches().add(matchOne);
        teamOne.getMatches().add(matchThree);
        teamTwo.getMatches().add(matchTwo);

        teamRepository.save(teamOne);
        teamRepository.save(teamTwo);

    }

    private Match createMatch(long homeTeamId, long awayTeamId, Long homeTeamGoals, Long awayTeamGoals,
                              int round, String stadiumName, int stadiumCapacity, int visitors) {
        Match match = new Match();
        match.setHomeTeamId(homeTeamId);
        match.setAwayTeamId(awayTeamId);
        match.setHomeTeamGoals(homeTeamGoals);
        match.setAwayTeamGoals(awayTeamGoals);
        match.setRound(round);
        match.setStadiumName(stadiumName);
        match.setStadiumCapacity(stadiumCapacity);
        match.setVisitors(visitors);

        return match;
    }

    private void loadEvents() {

        Events event = createEvent(10, false, true, false, matchOne, playerOne,
                false, false, false, false);
        eventsRepository.save(event);


        Events eventTwo = createEvent(15, false, false, false, matchOne, playerOne,
                false, true, false, false);
        eventsRepository.save(eventTwo);
    }

    private Events createEvent(int minute, boolean isAssist, boolean isGoal, boolean isOwnGoal, Match match,
                               Player player, boolean isRedCard, boolean isYellowCard, boolean isSubsIn,
                               boolean isSubsOut){

        Events event = new Events();
        event.setMinute(minute);
        event.setAssist(isAssist);
        event.setGoal(isGoal);
        event.setOwnGoal(isOwnGoal);
        event.setMatch(match);
        event.setPlayer(player);
        event.setRedCard(isRedCard);
        event.setYellowCard(isYellowCard);
        event.setSubstitutionIn(isSubsIn);
        event.setSubstitutionOut(isSubsOut);

        return event;
    }

}
