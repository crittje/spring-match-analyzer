package com.chris.matchanalyzer.services;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.repositories.EventsRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class EventsServiceImplTest {

    @Mock
    EventsRepository eventsRepository;

    EventsService eventsService;

    public EventsServiceImplTest() {
        this.eventsRepository= eventsRepository;
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        eventsService = new EventsServiceImpl(eventsRepository);
    }

    @Test
    public void getEventsByPlayerId() {

        Player player = new Player();
        player.setId(1L);

        Events eventOwnGoal = new Events();
        eventOwnGoal.setOwnGoal(true);
        eventOwnGoal.setPlayer(player);

        Events eventYellowCard = new Events();
        eventYellowCard.setPlayer(player);
        eventYellowCard.setYellowCard(true);

        List<Events> events = Arrays.asList(eventOwnGoal, eventYellowCard);

        when(eventsRepository.findPlayerEventsById(anyLong())).thenReturn(events);

        List<Events> returnedEvent = eventsService.getEventsByPlayerId(1L);

        assertNotNull("Null event returned", returnedEvent);
        assertEquals(2, returnedEvent.size());
        assertEquals(Long.valueOf(1L), returnedEvent.get(0).getPlayer().getId());
        assertEquals(Long.valueOf(1), returnedEvent.get(1).getPlayer().getId());
        assertTrue(returnedEvent.get(0).getOwnGoal());
        assertTrue(returnedEvent.get(1).getYellowCard());
    }
}