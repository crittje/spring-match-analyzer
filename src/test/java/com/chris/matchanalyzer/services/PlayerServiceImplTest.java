package com.chris.matchanalyzer.services;

import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.repositories.PlayerRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class PlayerServiceImplTest {

    @Mock
    PlayerRepository playerRepository;

    PlayerService playerService;

    public PlayerServiceImplTest() {
        this.playerRepository = playerRepository;
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        playerService = new PlayerServiceImpl(playerRepository);
    }


    @Test
    public void getAllDetailsTest() {

        Player player1 = new Player();

        Player player2 = new Player();

        List<Player> playerList = new ArrayList<>();
        playerList.add(player1);
        playerList.add(player2);

        when(playerService.getAllDetails()).thenReturn(playerList);

        List<Player> returnedPlayers = playerService.getAllDetails();

        assertEquals(2, returnedPlayers.size());
        verify(playerRepository, times(1)).findAll();
        verify(playerRepository, never()).findById(anyLong());

    }

    @Test
    public void getPlayerByIdTest() {

        Player player = new Player();
        player.setId(1L);
        Optional<Player> playerOptional = Optional.of(player);

        when(playerRepository.findById(anyLong())).thenReturn(playerOptional);

        Optional<Player> returnedPlayer = playerService.getPlayerById(1L);

        assertNotNull("Null player returned", returnedPlayer);
        assertEquals(Long.valueOf(1L), returnedPlayer.get().getId());
        verify(playerRepository, times(1)).findById(anyLong());
        verify(playerRepository, never()).findAll();
    }

    @Test
    public void getPlayerByName() {

        final String PLAYER_NAME = "Robben";

        Player player = new Player();
        player.setId(1L);
        player.setName(PLAYER_NAME);
        Optional<Player> playerOptional = Optional.of(player);

        when(playerRepository.findByName(anyString())).thenReturn(playerOptional);

        Optional<Player> returnedPlayer = playerService.getPlayerByName(PLAYER_NAME);

        assertNotNull("Null player returned", returnedPlayer);
        assertEquals(PLAYER_NAME, returnedPlayer.get().getName());
        verify(playerRepository, times(1)).findByName(anyString());
        verify(playerRepository, never()).findById(anyLong());
    }

    @Test
    public void getPlayerMatches() {
    }

}
