package com.chris.matchanalyzer.controllers;

import com.chris.matchanalyzer.domain.Events;
import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.services.EventsService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EventsControllerTest {

    @Mock
    private EventsService eventsService;

    @InjectMocks
    private EventsController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getEventsByPlayerId() throws Exception {

        Player player = new Player();
        player.setId(1L);

        Events eventOwnGoal = new Events();
        eventOwnGoal.setOwnGoal(true);
        eventOwnGoal.setPlayer(player);

        Events eventYellowCard = new Events();
        eventYellowCard.setPlayer(player);
        eventYellowCard.setYellowCard(true);

        List<Events> events = Arrays.asList(eventOwnGoal, eventYellowCard);

        when(eventsService.getEventsByPlayerId(anyLong())).thenReturn(events);

        mockMvc.perform(get("/events/player_id/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}