package com.chris.matchanalyzer.controllers;

import com.chris.matchanalyzer.domain.Player;
import com.chris.matchanalyzer.services.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlayerControllerTest {

    @Mock
    private PlayerService playerService;

    @InjectMocks
    private PlayerController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);


        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }


    @Test
    public void getPlayersAllDetailsTest() throws Exception {

        Player player1 = new Player();
        Player player2 = new Player();
        List<Player> playerList = new ArrayList<>();
        playerList.add(player1);
        playerList.add(player2);

        when(playerService.getAllDetails()).thenReturn(playerList);

        mockMvc.perform(get("/players/all_details")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getPlayerByIdTest() throws Exception {

        Player player = new Player();
        player.setId(1L);
        Optional<Player> playerOptional = Optional.of(player);

        when(playerService.getPlayerById(anyLong())).thenReturn(playerOptional);

        mockMvc.perform(get("/players/id/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(1)));
    }

    @Test
    public void getPlayerByNameTest() throws Exception {

        final String NAME = "Robben";

        Player player = new Player();
        player.setName(NAME);
        Optional<Player> playerOptional = Optional.of(player);

        when(playerService.getPlayerByName(anyString())).thenReturn(playerOptional);

        mockMvc.perform(get("/players/name/" + NAME)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(NAME)));
    }

//    @Test
//    public void getPlayerEventsByPlayerId() throws Exception {
//
//        Player player = new Player();
//        player.setId(1L);
//
//        Events eventOwnGoal = new Events();
//        eventOwnGoal.setOwnGoal(true);
//        eventOwnGoal.setPlayer(player);
//
//        Events eventYellowCard = new Events();
//        eventYellowCard.setPlayer(player);
//        eventYellowCard.setYellowCard(true);
//
//        List<Events> events = Arrays.asList(eventOwnGoal, eventYellowCard);
//        Optional<List<Events>> optionalEvents = Optional.of(events);
//
//        when(playerService.getPlayerEventsById(anyLong())).thenReturn(optionalEvents);
//
//        mockMvc.perform(get("/players/events_by_id/1").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
////                .andExpect(jsonPath("$"))
//        ;
//    }
}